#! /usr/bin/sh
# dbdumper.sh creates a SQL dump script in order
# Copyright (C) 2020 Pizzabanana

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

PROGRAM_NAME='dbdumper'
PROGRAM_DESCRIPTION='Creates a SQL dump script in order'
BASELINE_FILE='baseline.sql'
UPDATES_DIR='updates'
UPDATES_PATTERN='patch-[0-9]*-[0-9]*-[0-9]*.sql'
FUNCTIONS_DIR='functions'
FUNCTIONS_PATTERN='*.sql'
VIEWS_DIR='views'
VIEWS_PATTERN='*.sql'
TRIGGERS_DIR='triggers'
TRIGGERS_PATTERN='*.sql'

show_usage () {
    printf 'usage: %s [-h] -d DATABASE -s SCHEMA [-c]\n' "$PROGRAM_NAME" >&2
}

show_help () {
    show_usage
    printf '\n'
    printf '%s\n' "$PROGRAM_DESCRIPTION"
    printf '\n'
    printf 'optional arguments:\n'
    printf '  -h, --help            show this help message and exit\n'
    printf '  -d NAME, --database NAME\n'
    printf '                        database name\n'
    printf '  -s SCHEMA, --schema SCHEMA\n'
    printf '                        database schema\n'
    printf '  -c, --create          includes the create database statement\n'
}

load_script () {
    printf '%s File %s\n' "--" "$1"
    cat "$1" | sed -e "s/{{schema}}/$2/g"
    printf '\n'
}

load_scripts_folder () {
    scripts=$(find $1 -name "$2" | sort -)
    if [ -n "$scripts" ]; then
        for script in $scripts; do
            load_script "$script" "$3"
        done
    fi
}

database=''
schema=''
required_args=''
unrecognized_args=''
create_database=0

while :; do
    case $1 in
        -h|--help)
            show_help
            exit 0
            ;;
        -d|--database)
            if [ "$2" ]; then
                database="$2"
                shift
            else
                show_usage
                printf '%s: error: argument -d/--database: expected one argument\n' "$PROGRAM_NAME" >&2
                exit 2
            fi
            ;;
        -s|--schema)
            if [ "$2" ]; then
                schema="$2"
                shift
            else
                show_usage
                printf '%s: error: argument -s/--schema: expected one argument\n' "$PROGRAM_NAME" >&2
                exit 2
            fi
            ;;
        -c|--create)
            create_database=1
            ;;
        --)
            shift
            break
            ;;
        -?*)
            if [ -z "$unrecognized_args" ]; then
                unrecognized_args="$1"
            else
                unrecognized_args="$unrecognized_args $1"
            fi
            ;;
        *)
            break
    esac

    shift
done

if [ -n "$unrecognized_args" ]; then
    show_usage
    printf '%s: error: unrecognized arguments: %s\n' "$PROGRAM_NAME" "$unrecognized_args" >&2
    exit 2
fi

if [ -z "$database" ]; then
    required_args="$required_args -d/--database"
fi

if [ -z "$schema" ]; then
    required_args="$required_args -s/--schema"
fi

if [ -n "$required_args" ]; then
    show_usage
    printf '%s: error: the following arguments are required%s\n' "$PROGRAM_NAME" "$required_args" >&2
    exit 2
fi

if [ $create_database -eq 1 ]; then
    printf '%s Create database statement\n' "--"
    printf 'CREATE DATABASE %s;\n' "$database"
    printf '\n'
fi

# Connect to database
printf '%s Connect to database statement\n' "--"
printf '\\c %s\n' "$database"
printf '\n'

# Load baseline
if [ -f "$BASELINE_FILE" ]; then
    load_script "$BASELINE_FILE" "$schema"
fi

# Load updates
if [ -d "$UPDATES_DIR" ]; then
    load_scripts_folder "$UPDATES_DIR" "$UPDATES_PATTERN" "$schema"
fi

# Load objects
if [ -d "$FUNCTIONS_DIR" ]; then
    load_scripts_folder "$FUNCTIONS_DIR" "$FUNCTIONS_PATTERN" "$schema"
fi

if [ -d "$VIEWS_DIR" ]; then
    load_scripts_folder "$VIEWS_DIR" "$VIEWS_PATTERN" "$schema"
fi

if [ -d "$TRIGGERS_DIR" ]; then
    load_scripts_folder "$TRIGGERS_DIR" "$TRIGGERS_PATTERN" "$schema"
fi

exit 0
