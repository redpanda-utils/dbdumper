# DBDumper
> Script to create a dump file

DBDumper implements a python script to dump a SQL file to the last version.
This repository should be a git-submodule to database repositories.

* [Dump script](#dump-script)
* [Update database schema](#update-database-schema)
* [Update database functions](#update-database-functions)
* [Update database views](#update-database-views)
* [Update database triggers](#update-database-triggers)

## Dump script

To create the database script you should add the [baseline](baseline.sql), then
the pathces in order. To do this you can use the [dbdumper.py](dbdumper.py)
script that will dump the SQL script in order.

The usage is the following:

```bash
[user@host] $ /bin/sh dbdumper.sh -h
usage: dbdumper [-h] -d DATABASE -s SCHEMA [-c]

Creates a SQL dump script in order

optional arguments:
  -h, --help            show this help message and exit
  -d NAME, --database NAME
                        database name
  -s SCHEMA, --schema SCHEMA
                        database schema
  -c, --create          includes the create database statement
```

## Update database schema

To update database schema, create a new patch file, with the version name on the
updates directory: *patch-1-0.1.sql*, *patch-1-1-0.sql*, *patch-2-1-4.sql*.

Be sure to include at the end the insert of the database version:

```sql
INSERT INTO {{schema}}.schema_version VALUES(
    DEFAULT,
    major,
    minor,
    build,
    'Description of the patch',
    DEFAULT
);
```

**NEVER** edit one patch file that it's already merged on master branch. This
would cause database inconsistences. Create a new patch instead.

## Update database functions

To update functions or create new ones, just edit the corresponding files or
create a new one.

## Update database views

To update views or create new ones, just edit the corresponding files or
create a new one.

## Update database triggers

To update triggers or create new ones, just edit the corresponding files or
create a new one.
